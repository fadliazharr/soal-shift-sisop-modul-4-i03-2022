//1a
char *decodeVigenereCipher(char *cipherText) {
    int i;
    char text;
    char plainText[1001];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++) {
        if (cipherText[i] >= 65 && cipherText[i] <= 90) {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 65; 
            text = (char)textValue;
        } else if (cipherText[i] >= 97 && cipherText[i] <= 122) {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        } else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }
    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;
        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    } else {
        strcpy(fileName, encodeVigenereCipher(token));
    }
    strcpy(token, fileName);
    return token;
}

char *decodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');
    char fileName[1001];
    char result[1001] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }  else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }
    strcpy(token, result);
    return token;
}

//2a 
char *encodeVigenereCipher(char *plainText) {
    int i;
    char cipher;
    char cipherText[1001];
    int cipherValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(cipherText, 0, sizeof(cipherText));

    for (i = 0; i<strlen(plainText); i++) {
        if (plainText[i] >= 65 && plainText[i] <= 90) {
            cipherValue = (((int)plainText[i]-65) + (toupper(key[i%len])-65)) % 26 + 65;
            cipher = (char)cipherValue;
        } else if(plainText[i] >= 97 && plainText[i] <= 122) {
            cipherValue = (((int)plainText[i]-97) + (tolower(key[i%len])-97)) % 26 + 97;
            cipher = (char)cipherValue;
        } else
            cipher = plainText[i];
        strncat(cipherText, &cipher, 1);
    }
    strcpy(plainText, cipherText);
    return plainText;
}

char *decodeVigenereCipher(char *cipherText) {
    int i;
    char text;
    char plainText[1001];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++) {
        if (cipherText[i] >= 65 && cipherText[i] <= 90) {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 65; 
            text = (char)textValue;
        } else if (cipherText[i] >= 97 && cipherText[i] <= 122) {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        } else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }
    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;
        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    } else {
        strcpy(fileName, encodeVigenereCipher(token));
    }
    strcpy(token, fileName);
    return token;
}

char *decodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');
    char fileName[1001];
    char result[1001] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }  else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }
    strcpy(token, result);
    return token;
}

//2c 
char *decodeVigenereCipher(char *cipherText) {
    int i;
    char text;
    char plainText[1001];
    int textValue;
    char key[] = "INNUGANTENG";
    int len = strlen(key);
    memset(plainText, 0, sizeof(plainText));

    for (i = 0; i<strlen(cipherText); i++) {
        if (cipherText[i] >= 65 && cipherText[i] <= 90) {
            textValue = (((int)cipherText[i]-65) - (toupper(key[i%len])-65));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 65; 
            text = (char)textValue;
        } else if (cipherText[i] >= 97 && cipherText[i] <= 122) {
            textValue = (((int)cipherText[i]-97) - (tolower(key[i%len])-97));
            while (textValue < 0) {
                textValue += 26;
            }
            textValue += 97;
            text = (char)textValue;
        } else
            text = cipherText[i];
        strncat(plainText, &text, 1);
    }
    strcpy(cipherText, plainText);
    return cipherText;
}

char *encodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');

    char fileName[1001];
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;
        strncpy(fileName, token, length);
        fileName[length] = '\0';
        strcpy(fileName, encodeVigenereCipher(fileName));
        strcat(fileName, ext);
    } else {
        strcpy(fileName, encodeVigenereCipher(token));
    }
    strcpy(token, fileName);
    return token;
}

char *decodeFileIAN_(char *token) {
    char *ext = NULL;
    ext = strrchr(token, '.');
    char fileName[1001];
    char result[1001] = "";
    char *pt = NULL;
    if (ext) {
        int length = strlen(token) - strlen(ext);
        if(length < 0) length = 0;

        strncpy(fileName, token, length);
        fileName[length] = '\0';
        
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
        strcat(result, ext);
    }  else {
        strcpy(fileName, token);
        char *ptFileName = fileName;
        while((pt = strtok_r(ptFileName, "/", &ptFileName))) {
            strcat(result, "/");
            strcat(result, decodeVigenereCipher(pt));
        }
    }
    strcpy(token, result);
    return token;
}

//2d

int  main(int  argc, char *argv[]) {
    char *username = getenv("USER");
    char logPathSoal1[1001];
    char logPathSoal2[1001];

    sprintf(dirpath, "/home/%s/Documents", username);
    sprintf(logPathSoal1, "/home/%s/Wibu.log", username);
    sprintf(logPathSoal2, "/home/%s/hayolongapain_D10.log", username);
    
    fileLog1 = fopen(logPathSoal1, "w");
    fileLog2 = fopen(logPathSoal2, "w");
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

